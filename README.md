# 8-Bit Wonders Binaries

This repository contains reproducible builds that were built from https://gitlab.com/eightbitwonders/app.

If you simply want to download an apk to use it on your device, you're better of with the possibilites shown here: https://eightbitwonders.gitlab.io/app/

